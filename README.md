# Paul the Octopus player

CI&T Paul the Octopus 2021 player made using Google Cloud Functions and Google Firestore

## Local setup

Install and setup [NodeJS](https://nodejs.org/en/download/)

Install and setup Google Cloud SDK following the instructions [here](https://cloud.google.com/sdk/docs/install)

Verify setup

```bash
$ gcloud -v
```
```
Google Cloud SDK 346.0.0
bq 2.0.69
core 2021.06.18
gsutil 4.63
```

Install dependencies

```bash
$ npm install
```

Open `index.js` file and change line 15:

```js
exports.paulOctopusPlayerOne = async (req, res) => {
```

use your player name, for example:

```js
exports.paulOctopusOgura = async (req, res) => {
```

## Firestore setup

- Access GCP using Gemini as project using this [url](https://console.cloud.google.com/home/dashboard?project=phoenix-cit)
- Click on the Menu, and go to Firestore
- Add a new Document inside of the `predictions` collection
- The Document ID can be your player name (E.g `ogura`)
- Inside of this document, create a new Collection named `1` (this is the competition ID)
- Inside of the Collection `1`, add Documents using the Event ID as the Document ID for example 11 for Surfing.
- Inside of each Document with Event IDs add the following number fields:
```
gold: <Country ID>
silver: <Country ID>
bronze: <Country ID>
```

Go back to `index.js` and change line 32:

```js
const rootRef = firestore.collection("predictions").doc("player");
```

use your new collection name:

```js
const rootRef = firestore.collection("predictions").doc("ogura");
```

## Deployment

Deploy your function to GCP (If your nodeJS version is not >= 14, change the runtime bellow):
```bash
$ gcloud functions deploy paulOctopusOgura --runtime nodejs14 --trigger-http --allow-unauthenticated
```

This will create an HTTP callable function in GCP that can be triggered from this URL:
```
https://us-central1-phoenix-cit.cloudfunctions.net/paulOctopusOgura
```

Set your secret as an environment variable, so it won't be in any repo:

```bash
$ gcloud functions deploy paulOctopusOgura --set-env-vars MY_SECRET=<your_secret>
```

## Testing

Call your function using POST in any REST client. [Postman](https://www.postman.com/downloads/) or [ARC](https://chrome.google.com/webstore/detail/advanced-rest-client/hgmloofddffdnphfgcellkdfbfbjeloo?hl=en-US)

- URL:
```
https://us-central1-phoenix-cit.cloudfunctions.net/paulOctopusOgura
```

- Header:

```
Content-Type = application/json
SECRET = <your_secret>
```

- Body JSON for normal test:
```json
{
  "test_mode": true,
  "id_league": 1,
  "id_competition": 1,
  "id_event": 1,
  "id_player": 1
}
```

- Body JSON for TWIST test:
```json
{
  "test_mode": false,
  "id_league": 1,
  "id_competition": 1,
  "id_event": 1,
  "id_player": 1,
  "predictions": {
    "gold": {
      "23": 0.5,
      "123": 0.2,
      "1": 0.1,
      "2": 0.3
    },
    "silver": {
      "23": 0.5,
      "123": 0.2,
      "1": 0.1,
      "2": 0.3
    },
    "bronze": {
      "23": 0.5,
      "123": 0.2,
      "1": 0.1,
      "2": 0.3
    }
  }
}
```

Remember to change the `test_mode` field to validate your real results as well.

Using `Paul Core API` to test:

- URL:
```
https://pto-core-api-dot-phoenix-cit.wl.r.appspot.com/testapi/paultheoctopus
```

- Header:

```
Content-Type = application/json
SECRET = <your_secret>
```

- Body JSON for normal test:
```json
{
  "test_mode": true,
  "id_league": 1,
  "id_competition": 1,
  "id_event": 1,
  "twist": false
}
```