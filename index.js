const Firestore = require('@google-cloud/firestore');
const firestore = new Firestore({
  projectId: process.env.GCP_PROJECT,
  timestampsInSnapshots: true
});

/**
 * HTTP Cloud Function.
 *
 * @param {Object} req Cloud Function request context.
 *                     More info: https://expressjs.com/en/api.html#req
 * @param {Object} res Cloud Function response context.
 *                     More info: https://expressjs.com/en/api.html#res
 */
exports.paulOctopusPlayerOne = async (req, res) => {
    const bodyJson = req.body;
    const headerJson = req.headers;

    var result = {};
    if (headerJson.secret != process.env.MY_SECRET) {
        res.status(503).send(result);
    } else {
        if (bodyJson.test_mode) {
            result = {
                "gold": 1,
                "silver": 1,
                "bronze": 1
            };
        } else {
            if (!bodyJson.predictions) {
                try {
                    const rootRef = firestore.collection("predictions").doc("player");
                    const docRef = rootRef.collection(bodyJson.id_competition.toString()).doc(bodyJson.id_event.toString());
                    const docSnapshot = await docRef.get();
                    if (!(docSnapshot && docSnapshot.exists)) {
                        return res.status(404).send({ error: `Unable to find the prediction for competition_id = ${bodyJson.id_competition} and event_id = ${bodyJson.id_event}` });
                    }
                    const data = docSnapshot.data();
                    result = {
                        "gold": data.gold,
                        "silver": data.silver,
                        "bronze": data.bronze
                    };
                } catch (error) {
                    console.error(new Error(`Predictions could not be found due to: ${error}`));
                    return res.status(404).send({ error: `Unable to find the prediction for event_id = ${bodyJson.id_event}` });
                }
            } else { // TWIST
                result = {
                    "gold": getMax(bodyJson.predictions.gold),
                    "silver": getMax(bodyJson.predictions.silver),
                    "bronze": getMax(bodyJson.predictions.bronze)
                }
            }
        }
    
        res.status(200).send(result);
    }
};

function getMax(obj) {
    return Object.keys(obj).reduce((a, b) => obj[a] > obj[b] ? a : b);
}